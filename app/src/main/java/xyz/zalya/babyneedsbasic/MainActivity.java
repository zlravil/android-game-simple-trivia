package xyz.zalya.babyneedsbasic;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import xyz.zalya.babyneedsbasic.util.RecyclerViewAdapter;
import xyz.zalya.babyneedsbasic.data.BabyNeedsDbHelper;
import xyz.zalya.babyneedsbasic.model.BabyItem;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private BabyNeedsDbHelper dbHelper;

    private AlertDialog.Builder dialogBuider;
    private AlertDialog dialog;
    private Button saveButton;
    private EditText babyItem;
    private EditText itemQuantity;
    private EditText itemColor;
    private EditText itemSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        dbHelper = new BabyNeedsDbHelper(MainActivity.this);
        // dbHelper.seeder();

        byPassActivity();

        final List<BabyItem> itemsList = dbHelper.getAllItems();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPopupDialog();
/*                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

    }

    private void byPassActivity() {
        if (dbHelper.getItemsCount() > 0) {
            startActivity(new Intent(MainActivity.this, ListActivity.class));
            finish();
        }
    }


    private void createPopupDialog() {
        dialogBuider = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.popup, null);
        babyItem = view.findViewById(R.id.item_name);
        itemQuantity = view.findViewById(R.id.item_quantity);
        itemSize = view.findViewById(R.id.item_size);
        itemColor = view.findViewById(R.id.item_color);
        saveButton = view.findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!babyItem.getText().toString().isEmpty() &&
                        !itemColor.getText().toString().isEmpty() &&
                        !itemQuantity.getText().toString().isEmpty() &&
                        !itemSize.getText().toString().isEmpty()) {
                    Snackbar.make(view, "Item Saved", Snackbar.LENGTH_SHORT).show();
                    saveItem(view);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                            startActivity(new Intent(MainActivity.this, ListActivity.class));
                        }
                    }, 1200);
                } else {
                    Snackbar.make(view, "Empty Fields Not Allowed", Snackbar.LENGTH_SHORT).show();
                }

            }
        });
        dialogBuider.setView(view);
        dialog = dialogBuider.create();
        dialog.show();
    }

    private void saveItem(View view) {
        BabyItem item = new BabyItem();

        String babyItemName = babyItem.getText().toString().trim();
        int babyItemQuantity = Integer.parseInt(itemQuantity.getText().toString().trim());
        int babyItemSize = Integer.parseInt(itemSize.getText().toString().trim());
        String babyItemColor = itemColor.getText().toString().trim();

        item.setName(babyItemName);
        item.setQuantity(babyItemQuantity);
        item.setSize(babyItemSize);
        item.setColor(babyItemColor);

        dbHelper.addItem(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}