package xyz.zalya.babyneedsbasic.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.text.MessageFormat;
import java.util.List;

import xyz.zalya.babyneedsbasic.R;
import xyz.zalya.babyneedsbasic.data.BabyNeedsDbHelper;
import xyz.zalya.babyneedsbasic.model.BabyItem;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<BabyItem> itemList;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private LayoutInflater inflater;

    public RecyclerViewAdapter(Context context, List<BabyItem> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_row, viewGroup, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder viewHolder, int position) {

        BabyItem item = itemList.get(position); // object Item

        viewHolder.itemName.setText(MessageFormat.format("Item: {0}", item.getName()));
        viewHolder.itemColor.setText(MessageFormat.format("Color: {0}", item.getColor()));
        viewHolder.quantity.setText(MessageFormat.format("Qty: {0}", String.valueOf(item.getQuantity())));
        viewHolder.size.setText(MessageFormat.format("Size: {0}", String.valueOf(item.getSize())));
        viewHolder.dateAdded.setText(MessageFormat.format("Added on: {0}", item.getDate()));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView itemName;
        public TextView itemColor;
        public TextView quantity;
        public TextView size;
        public TextView dateAdded;
        public Button editButton;
        public Button deleteButton;

        public int id;

        public ViewHolder(@NonNull View itemView, Context ctx) {
            super(itemView);
            context = ctx;

            itemName = itemView.findViewById(R.id.item_name);
            itemColor = itemView.findViewById(R.id.item_color);
            quantity = itemView.findViewById(R.id.item_quantity);
            size = itemView.findViewById(R.id.item_size);
            dateAdded = itemView.findViewById(R.id.item_date);

            editButton = itemView.findViewById(R.id.editButton);
            deleteButton = itemView.findViewById(R.id.deleteButton);

            editButton.setOnClickListener(this);
            deleteButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int position;
            position = getAdapterPosition();
            BabyItem item = itemList.get(position);

            switch (v.getId()) {
                case R.id.editButton:
                    //edit item
                    editItem(item);
                    break;
                case R.id.deleteButton:
                    //delete item
                    deleteItem(item.getId());
                    break;
            }
        }

        private void deleteItem(final int id) {

            builder = new AlertDialog.Builder(context);

            inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.confirmation_pop, null);

            Button noButton = view.findViewById(R.id.conf_no_button);
            Button yesButton = view.findViewById(R.id.conf_yes_button);

            builder.setView(view);
            dialog = builder.create();
            dialog.show();


            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BabyNeedsDbHelper db = new BabyNeedsDbHelper(context);
                    db.deleteItem(id);
                    itemList.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());

                    dialog.dismiss();
                }
            });
            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }

        private void editItem(final BabyItem newItem) {

            builder = new AlertDialog.Builder(context);
            inflater = LayoutInflater.from(context);
            final View view = inflater.inflate(R.layout.popup, null);

            Button saveButton;
            final EditText babyItem;
            final EditText itemQuantity;
            final EditText itemColor;
            final EditText itemSize;
            TextView title;

            babyItem = view.findViewById(R.id.itemName);
            itemQuantity = view.findViewById(R.id.itemQuantity);
            itemColor = view.findViewById(R.id.itemColor);
            itemSize = view.findViewById(R.id.itemSize);
            saveButton = view.findViewById(R.id.saveButton);
            saveButton.setText(R.string.update_text);
            title = view.findViewById(R.id.title);

            title.setText(R.string.edit_time);
            babyItem.setText(newItem.getName());
            itemQuantity.setText(String.valueOf(newItem.getQuantity()));
            itemColor.setText(newItem.getColor());
            itemSize.setText(String.valueOf(newItem.getSize()));

            builder.setView(view);
            dialog = builder.create();
            dialog.show();

            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //update our item
                    BabyNeedsDbHelper databaseHandler = new BabyNeedsDbHelper(context);

                    //update items
                    newItem.setName(babyItem.getText().toString());
                    newItem.setColor(itemColor.getText().toString());
                    newItem.setQuantity(Integer.parseInt(itemQuantity.getText().toString()));
                    newItem.setSize(Integer.parseInt(itemSize.getText().toString()));

                    if (!babyItem.getText().toString().isEmpty()
                            && !itemColor.getText().toString().isEmpty()
                            && !itemQuantity.getText().toString().isEmpty()
                            && !itemSize.getText().toString().isEmpty()) {

                        databaseHandler.updateItem(newItem);
                        notifyItemChanged(getAdapterPosition(), newItem); //important!

                    } else {
                        Snackbar.make(view, "Fields Empty",
                                Snackbar.LENGTH_SHORT)
                                .show();
                    }
                    dialog.dismiss();
                }
            });
        }
    }

/*    private Context context;
    private List<BabyItem> itemList;

    public RecyclerViewAdapter(Context context, List<BabyItem> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BabyItem babyItem = itemList.get(position);
        holder.name.setText("Name: " + babyItem.getName());
        holder.quantity.setText("Quantity: " + String.valueOf(babyItem.getQuantity()));
        holder.color.setText("Color: " + babyItem.getColor());
        holder.size.setText("Size: " + String.valueOf(babyItem.getSize()));
        holder.date.setText("Date: " + babyItem.getDate());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private TextView quantity;
        private TextView color;
        private TextView size;
        private TextView date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.view_name);
            quantity = itemView.findViewById(R.id.view_quantity);
            color = itemView.findViewById(R.id.view_color);
            size = itemView.findViewById(R.id.view_size);
            date = itemView.findViewById(R.id.view_date);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int idx = getAdapterPosition();
            BabyItem babyIitem = itemList.get(idx);

            createPopupDialog();
            
            Intent intent = new Intent(context, ListActivity.class);
            intent.putExtra("name", babyIitem.getName());
            intent.putExtra("quantity", babyIitem.getQuantity());
            intent.putExtra("color", babyIitem.getColor());
            intent.putExtra("size", babyIitem.getSize());
            intent.putExtra("date", babyIitem.getDate());
            context.startActivity(intent);
        }

        private void createPopupDialog() {
        }
    }*/
}
