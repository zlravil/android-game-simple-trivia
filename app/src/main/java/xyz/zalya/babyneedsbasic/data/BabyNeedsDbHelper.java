package xyz.zalya.babyneedsbasic.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import xyz.zalya.babyneedsbasic.model.BabyItem;
import xyz.zalya.babyneedsbasic.util.Util;

public class BabyNeedsDbHelper extends SQLiteOpenHelper {

    private final Context context;

    public BabyNeedsDbHelper(@Nullable Context context) {
        super(context, Util.DATABASE_NAME, null, Util.DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_BABYITEM_TABLE = "create table " + Util.TABLE_NAME + " ("
                + Util.KEY_ID + " INTEGER PRIMARY KEY, "
                + Util.KEY_NAME + " TEXT, "
                + Util.KEY_COLOR + " TEXT, "
                + Util.KEY_QUANTITY + " INTEGER, "
                + Util.KEY_SIZE + " INTEGER, "
                + Util.KEY_DATE + " LONG );";
        sqLiteDatabase.execSQL(CREATE_BABYITEM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String DROP_TABLE = "DROP TABLE IF EXISTS " + Util.TABLE_NAME;
        sqLiteDatabase.execSQL(DROP_TABLE, new String[]{Util.DATABASE_NAME});
        onCreate(sqLiteDatabase);
    }

    public BabyItem getItem(int id) {
        SQLiteDatabase dbRead = getReadableDatabase();
        String selectById = "SELECT * from " + Util.TABLE_NAME + " where id=" + id;
        Cursor cursor = dbRead.rawQuery(selectById, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        BabyItem babyItem = new BabyItem();
        babyItem.setId(cursor.getInt(cursor.getColumnIndex(Util.KEY_ID)));
        babyItem.setName(cursor.getString(cursor.getColumnIndex(Util.KEY_NAME)));
        babyItem.setColor(cursor.getString(cursor.getColumnIndex(Util.KEY_COLOR)));
        babyItem.setQuantity(cursor.getInt(cursor.getColumnIndex(Util.KEY_QUANTITY)));
        babyItem.setSize(cursor.getInt(cursor.getColumnIndex(Util.KEY_SIZE)));
        DateFormat dateFormat = DateFormat.getInstance();
        String date = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Util.KEY_DATE))).getTime());
        babyItem.setDate(date);
        return babyItem;
    }

    public List<BabyItem> getAllItems() {
        SQLiteDatabase dbRead = getReadableDatabase();
        List<BabyItem> itemsList = new ArrayList<>();
        // String selectAll = "SELECT * from " + Util.TABLE_NAME;
        Cursor cursor = dbRead.query(Util.TABLE_NAME, new String[]{
                        Util.KEY_ID,
                        Util.KEY_NAME,
                        Util.KEY_COLOR,
                        Util.KEY_QUANTITY,
                        Util.KEY_SIZE,
                        Util.KEY_DATE},
                null,
                null,
                null,
                null,
                Util.KEY_DATE + " DESC");
        if (cursor.moveToFirst()) {
            do {
                BabyItem babyItem = new BabyItem();
                babyItem.setId(cursor.getInt(cursor.getColumnIndex(Util.KEY_ID)));
                babyItem.setName(cursor.getString(cursor.getColumnIndex(Util.KEY_NAME)));
                babyItem.setColor(cursor.getString(cursor.getColumnIndex(Util.KEY_COLOR)));
                babyItem.setQuantity(cursor.getInt(cursor.getColumnIndex(Util.KEY_QUANTITY)));
                babyItem.setSize(cursor.getInt(cursor.getColumnIndex(Util.KEY_SIZE)));

                DateFormat dateFormat = DateFormat.getInstance();
                String date = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Util.KEY_DATE))).getTime());
                babyItem.setDate(date);

                itemsList.add(babyItem);
            }
            while (cursor.moveToNext());
        }
        return itemsList;
    }

    public void addItem(BabyItem babyItem) {
        SQLiteDatabase dbWrite = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Util.KEY_NAME, babyItem.getName());
        values.put(Util.KEY_COLOR, babyItem.getColor());
        values.put(Util.KEY_QUANTITY, babyItem.getQuantity());
        values.put(Util.KEY_SIZE, babyItem.getSize());
        values.put(Util.KEY_DATE, java.lang.System.currentTimeMillis());
        dbWrite.insert(Util.TABLE_NAME, null, values);
        Log.d("Main", "OnSave: " + values.getAsString(Util.KEY_NAME));
    }

    public int updateItem(BabyItem babyItem) {
        SQLiteDatabase dbWrite = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Util.KEY_NAME, babyItem.getName());
        values.put(Util.KEY_COLOR, babyItem.getColor());
        values.put(Util.KEY_QUANTITY, babyItem.getQuantity());
        values.put(Util.KEY_SIZE, babyItem.getSize());
        values.put(Util.KEY_DATE, babyItem.getDate());
        return dbWrite.update(Util.TABLE_NAME, values, Util.KEY_ID + "=?", new String[]{String.valueOf(babyItem.getId())});
    }

    public int deleteItem(int id) {
        SQLiteDatabase dbWrite = getWritableDatabase();
        return dbWrite.delete(Util.TABLE_NAME, Util.KEY_ID + "=?", new String[]{String.valueOf(id)});
    }

    public int getItemsCount() {
        String countQuery = "SELECT * FROM " + Util.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }

    public void seeder() {
        for (int i = 1; i < 20; i++) {
            deleteItem(i);
        }
//        addItem(new BabyItem("Baby powder", 3, "White", 1, "05-01-2020"));
//        addItem(new BabyItem("Napkins", 10, "Green", 2, "05-07-2020"));
//        addItem(new BabyItem("Socks", 5, "Blue", 2, "05-04-2020"));
//        addItem(new BabyItem("Hat", 2, "Blue", 2, "05-09-2020"));
//        addItem(new BabyItem("Chair", 1, "Brown", 2, "05-10-2020"));
//        addItem(new BabyItem("Matress", 1, "White", 3, "05-13-2020"));
//        addItem(new BabyItem("Oil", 4, "Pink", 3, "05-17-2020"));
    }
}
